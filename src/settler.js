class Settler {

  //------------- Конструктор
  constructor() {
    $("#zsForm").on("submit", (e) => {
      e.preventDefault()

      this.setZh(e.target)
    })
  }


  //------------- Message
  message(message) {
    if (this.messageTimer) {
      clearTimeout(this.messageTimer);
    }

    $(".message").text(message);

    this.messageTimer = setTimeout(() => { $(".message").html("&nbsp;") }, 1000);
  }


  //------------- Set Zh
  setZh(form) {
    let dataSet = {
      id: Number($("[name='id']", $(form)).val()),
      value: $("[name='value']", $(form)).val()
    }

    $("[type='submit']", $(form)).attr("disabled", true)

    $.ajax({
      url: "setzh.php",
      type: "POST",
      data: dataSet,
      dataType: "json",
      
      success: (data) => {
        $("[type='submit']", $(form)).attr("disabled", false)
        if ((typeof(data) == "object") && (data != null) && (data.code == "OK")) {
          this.message("Done")
        } else {
          let message = ((typeof(data) == "object") && (data != null) && (typeof(data.message) == "string")) ? $.trim(data.message) : ""
          console.log(message ? message : "Unknown error")
          this.message(message ? message : "Unknown error")
        }
      },
      
      error: (XMLHttpRequest, textStatus, errorThrown) => {
        console.log(XMLHttpRequest, textStatus, errorThrown)
        $("[type='submit']", $(form)).attr("disabled", false)
        this.message(errorThrown ? errorThrown : "Ajax error")
      }
    });


  }
}

export default Settler
