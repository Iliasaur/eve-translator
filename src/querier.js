import langs from "../json/langs.json"

class Querier {

  //------------- Конструктор
  constructor() {
    this.langFrom = langs[3]
    this.langTo = langs[0]
    this.messengerTimer = 0
    this.isMobileMode = false

    this.resize()
    $(window).on("resize", () => {
      if (this.resizeTimer) { clearTimeout(this.resizeTimer) }
      setTimeout(() => { this.resize() }, 200)
    })

    let storage = null
    try {
      storage = JSON.parse(localStorage["et"])
    } catch (err) {
      storage = null
    }

    if ((typeof(storage) == "object") && (storage != null)) {
      if (!isNaN(storage.langFrom) && (Number(storage.langFrom) >= 0) && (Number(storage.langFrom) < langs.length)) { this.langFrom = langs[Number(storage.langFrom)]; }
      if (!isNaN(storage.langTo) && (Number(storage.langTo) >= 0) && (Number(storage.langTo) < langs.length)) { this.langTo = langs[Number(storage.langTo)]; }
      if (storage.expandResults) { $(".expandResults").addClass("checked") }
      if (storage.useEnter) { $(".useEnter").addClass("checked") }
      if (storage.clearAfter) { $(".clearAfter").addClass("checked") }
    }

    this.highlightLangFrom();  
    this.highlightLangTo();

    $(".from .language").on("click", (e) => { this.setLanguageFrom(e) })
    $(".to .language").on("click", (e) => { this.setLanguageTo(e) })
    $(".left .btnClear").on("click", this.clearInput)

    $("#queryForm").on("submit", (e) => {
      e.preventDefault()

      let key = $("textarea[name='key']").val()
      this.query(key)
    })

    $("textarea[name='key']").on("keydown", (e) => {
      if (!$(".useEnter").hasClass("checked")) { return }
      let keyCode = (typeof(e.keyCode) == "number") && !isNaN(e.keyCode) ? e.keyCode : 0
      if (keyCode == 13) {
        let key = $("textarea[name='key']").val()
        if (key) { this.query(key, true) }
      }
    })

    $(".mgBtnSearch").on("click", () => {
      if ($(".mgSearchbar").hasClass("disabled")) { return; }
      this.focus()
      let key = $("input[name='key']").val()
      if (key) { this.query(key, true) }
    });

    $("input[name='key']").on("keyup", (event) => {
      if ($(".mgSearchbar").hasClass("disabled")) { return; }
      event = event || window.event;
      let key = $("input[name='key']").val()

      if (event.keyCode == 13) {
        if (key) { this.query(key, true) }
        return;
      }

      if (key) { $(".mgSearchbar").addClass("fill"); } else { $(".mgSearchbar").removeClass("fill"); }
    });

    $(".mgBtnClear").on("click", () => {
      if ($(".mgSearchbar").hasClass("disabled")) { return; }
      this.clearInput();
    });

    $(".mgCheckbox").on("click", (e) => {
      let parent = $(e.target).hasClass(".mgCheckbox") ? $(e.target) : $(e.target).parent(".mgCheckbox")
      if (parent.hasClass("disabled")) { return; }
      if (parent.hasClass("checked")) { parent.removeClass("checked"); } else { parent.addClass("checked"); }
      $("a", parent).blur()
      this.store()
    });
  }


  //------------- Message
  message(message) {
    $(".scroller").empty()
    $("<div />").addClass("error").text(message).appendTo($(".scroller"))
  }


  //------------- Query
  query(key) {
    this.focus()

    this.detectLang(key)

    if ((typeof(key) != "string") || !$.trim(key)) {
      return
    }

    $("input[type='submit']").attr("disabled", true)
    $(".mgSearchbar").addClass("disabled");

    let splitted = key.split("\n").filter(part => $.trim(part));

    let i = 0;
    while (i < splitted.length - 1) {
      let j = i + 1
      while (j < splitted.length) {
        if (splitted[i] == splitted[j]) {
          splitted.splice(j, 1)
        } else {
          j++
        }
      }
      i++
    }

    let filteredKey = []
    for (let part of splitted) {
      if (!$.trim(part)) { continue }
      filteredKey.push($.trim(part))
      if (filteredKey.length >= 10) { break }
    }

    let dataSet = {
      key: filteredKey.map(value => ({value, from: this.langFrom.id < 3 ? this.langFrom.id : this.detectLang(value)}))
    }

    $(".scroller").empty();
    $("<div />").addClass("circonf").css({marginTop: "100px"}).appendTo($(".scroller"))

    $.ajax({
      url: "query.php",
      type: "POST",
      data: dataSet,
      dataType: "json",
      
      success: (data) => {
        console.log(data)
        $("input[type='submit']").attr("disabled", false)
        $(".mgSearchbar").removeClass("disabled");
        if ((typeof(data) == "object") && (data != null) && (data.code == "OK")) {
          this.put(data)
        } else {
          let message = ((typeof(data) == "object") && (data != null) && (typeof(data.message) == "string")) ? $.trim(data.message) : ""
          console.log(message ? message : "Unknown error")
          this.message(message ? message : "Unknown error")
        }
      },
      
      error: (XMLHttpRequest, textStatus, errorThrown) => {
        console.log(XMLHttpRequest, textStatus, errorThrown)
        $("input[type='submit']").attr("disabled", false)
        $(".mgSearchbar").removeClass("disabled");
        this.message(errorThrown ? errorThrown : "Ajax error")
      }
    });

    if (!this.isMobileMode && $(".clearAfter").hasClass("checked")) {
      setTimeout(() => { this.clearInput() }, 200)
    }
  }



  //------------- Put Result
  put(result) {
    $(".scroller").empty();

    if (
      (typeof(result) != "object") || (result == null)
      || (!$.isArray(result.key) || (result.key.length < 1))
      || !$.isArray(result.translates)
    ) {
      this.message("Unable to parse translation result")
      return
    }

    let $result = $("<div />").addClass("result")
    let results = 0;

    for (let translate of result.translates) {
      if ((typeof(translate) != "object") || (translate == null) || (typeof(translate.key) != "string") || !$.trim(translate.key) || isNaN(translate.from) || (translate.from < 0) || (translate.from > 2)) { continue; }

      let lang = langs[translate.from]

      let matches = 0
      let $translation = $("<div />").addClass("translation")
      if (this.isMobileMode || $(".expandResults").hasClass("checked")) { $translation.addClass("preExpanded") }
      $("<div />").addClass("key").text(translate.key).appendTo($translation)
      let $stat = $("<div />").addClass("stat")
      let $direction = $("<div />").addClass("direction")
      $("<div />").addClass("from " + lang.short.toLowerCase()).appendTo($direction)
      $("<div />").addClass("arrow").text("→").appendTo($direction)
      $("<div />").addClass("to " + this.langTo.short.toLowerCase()).appendTo($direction)
      $direction.appendTo($stat)
      $("<div />").addClass("matches").text("Matches: 0").appendTo($stat)
      $("<div />").addClass("expander").on("click", (e) => { this.expand(e.target) }).appendTo($stat)
      $stat.appendTo($translation)
      let $rest = null;
      
      for (let row of translate.rows) {
        if ((typeof(row) != "object") || (row == null) || isNaN(row.id)) { continue; }
        let hasEnglish = ((typeof(row.en) == "string") && $.trim(row.en))
        let hasRussian = ((typeof(row.ru) == "string") && $.trim(row.ru))
        let hasChinese = ((typeof(row.zh) == "string") && $.trim(row.zh))
        if (!hasEnglish && !hasRussian && !hasChinese) { continue }

        let langsCount = 0;
        if (hasEnglish) { langsCount++ }
        if (hasRussian) { langsCount++ }
        if (hasChinese) { langsCount++ }

        let hasToLang = (this.langTo.id < 3) && ((typeof(row[this.langTo.short.toLowerCase()]) == "string") && $.trim(row[this.langTo.short.toLowerCase()]));
        if (!hasToLang && (this.langTo.id < 3)) { continue }
        if (hasToLang && (langsCount < 2)) { continue }

        let hasFromLang = ((typeof(row[lang.short.toLowerCase()]) == "string") && $.trim(row[lang.short.toLowerCase()]));
        if (hasFromLang && (langsCount < 2)) { continue }

        let $match = $("<div />").addClass("match").attr("data-id", row.id)
        let $num = $("<div />").addClass("num").appendTo($match)
        $("<span />").addClass("value").text(matches + 1).appendTo($num)
        let $triada = $("<div />").addClass("triada").appendTo($match)

        for (let lang of langs) {
          if (lang.id > 2) { continue }
          let text = row[lang.short.toLowerCase()];
          if ((matches < 1) && (lang.id == this.langFrom.id) && (this.langFrom.id != this.langTo.id) && (text.toUpperCase() == translate.key.toUpperCase())) { continue }
          if ((this.langTo.id > 2) || (this.langTo.id == lang.id)) {
            let $cont = $("<div />").addClass("lang" + (this.langTo.id > 2 ? " " + lang.short.toLowerCase() : ""))
            $("<span />").addClass("ind").text(lang.short + ": ").appendTo($cont)
            $("<span />").addClass("text" + (text ? "" : " empty")).text(text ? text : "—").appendTo($cont)
            $cont.appendTo($triada)
          }
        }

        if (matches < 1) {
          results++
          $match.addClass("first").appendTo($translation)
        } else {
          if (!$rest) { $rest = $("<div />").addClass("rest") }
          $match.appendTo($rest)
        }
        matches++
      }
 
      $(".matches", $translation).text("Matches: " + matches)
      if (matches < 1) { $stat.addClass("zero") } else {
        if (matches < 2) { $stat.addClass("single") }
      }

      if ($rest) { $rest.appendTo($translation) }
      $translation.appendTo($result)
    }

    if (results > 0) {
      $result.appendTo($(".scroller"))
    } else {
      let zero = $("<div />").addClass("zero").appendTo($(".scroller"))
      $("<div />").text("Nothing found").appendTo(zero)
      $("<div />").addClass("keyword label").text(result.key.length < 2 ? "Keyword:" : "Multiple keyword").appendTo(zero)
      if (result.key.length < 2) {
        $("<div />").addClass("keyword value").text(result.key[0].value).appendTo(zero)
      }
    }
  }


  //------------- Set Zh
  detectLang(key) {
    if (/[а-яА-ЯЁё]/.test(key)) { return 1; }
    let en = "1234567890`~!@#$%^&*()_+-=qwertyuiop[]{}asdfghjkl;'\\ |zxcvbnm,./<>?\r\n";
    for (let ch of key.toLowerCase()) {
      if (en.indexOf(ch) < 0) { return 2 }
    }
    return 0
  }



  /****************************************************************************

              SOMETHING ELSE  

  ****************************************************************************/

  //------------- Возвращает язык по краткому наименованию
  langByShort(short) {
    for (let lang of langs) {
      if (lang.short == short) { return lang }
    }
    return null
  }


  //------------- Фокус на ввод
  focus() {
    if (this.isMobileMode) {
      $(".mgSearchbar input[name='key']").focus()      
    } else {
      $("textarea[name='key']").focus()  
    }
  }


  //------------- Установка langFrom
  setLanguageFrom(e) {
    let langId = Number($(e.target).attr("data-value"))
    for (let lang of langs) {
      if (lang.id === langId) {
        this.langFrom = lang
        this.highlightLangFrom()
        break
      }
    }
    this.store()
    this.focus()
  }


  //------------- Установка langTo
  setLanguageTo(e) {
    let langId = Number($(e.target).attr("data-value"))
    for (let lang of langs) {
      if (lang.id === langId) {
        this.langTo = lang
        this.highlightLangTo()
        break
      }
    }
    this.store()
    this.focus()
  }


  //------------- Подсвечиваем langFrom
  highlightLangFrom() {
    $(".from .language").each((index, element) => {
      if ($(element).attr("data-value") == this.langFrom.id) {
        $(element).addClass("selected") 
      } else {
        $(element).removeClass("selected") 
      }
    });
  }


  //------------- Подсвечиваем langTo
  highlightLangTo() {
    $(".to .language").each((index, element) => {
      if ($(element).attr("data-value") == this.langTo.id) {
        $(element).addClass("selected") 
      } else {
        $(element).removeClass("selected") 
      }
    });
  }


  //------------- Очищаем ввод
  clearInput() {
    $("[name='key']").val("")
    this.focus()
  }


  //------------- Разворачивает перевод
  expand(el) {
    let $translation = $(el).parents(".translation")
    if ($translation.hasClass("expanded") || $translation.hasClass("preExpanded")) {
      $(".rest", $translation).slideUp()
      $translation.removeClass("expanded preExpanded")
    } else {
      $(".rest", $translation).slideDown()
      $translation.addClass("expanded")
    }
  }


  //------------- Resize
  resize() {
    if ($(window).width() <= 800) {
      this.isMobileMode = true
      $(".cover").addClass("mobile")
    } else {
      this.isMobileMode = false
      $(".cover").removeClass("mobile")
    }

    $(".scroller").height(
      $(window).height() - $(".scroller").offset().top - 24
    )
    
    if (!this.isMobileMode) { this.focus() }
  }


  //------------- Запоминает настройки
  store() {
    localStorage["et"] = JSON.stringify({
      langFrom: this.langFrom.id,
      langTo: this.langTo.id,
      expandResults: $(".expandResults").hasClass("checked"),
      useEnter: $(".useEnter").hasClass("checked"),
      clearAfter: $(".clearAfter").hasClass("checked")
    });
  }
}

export default Querier
