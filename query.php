<?
require_once($_SERVER["DOCUMENT_ROOT"]."/system/info.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/system/utils.php");

//  Подготовка полей

if (!isset($_POST["key"]) || !is_array($_POST["key"])) {
  logAndExit("Unable to get keyword");
}

$key = [];

for ($i = 0; $i < count($_POST["key"]); $i++) {
  if (!isset($_POST["key"][$i]["value"]) || !isset($_POST["key"][$i]["from"])) { continue; }
  if (gettype($_POST["key"][$i]["value"]) != "string") { continue; }
  $st = trim(strip_tags($_POST["key"][$i]["value"]));
  $from = ValidateInteger(trim($_POST["key"][$i]["from"]), false, false);
  if (!empty($st) && ($from >= 0) && ($from < 3)) {
    array_push(
      $key,
      array(
        "value" => $st,
        "from" => $from
      )
    );
  }
}

if (count($key) < 1) {
  logAndExit("Got empty keyword");
}




//  Соединение с mySQL

$mysql = Start(true);




//   Переводим

$translates = [];

for ($n = 0; $n < count($key); $n++) {
  
  $part = array(
    "key" => $key[$n]["value"],
    "from" => $key[$n]["from"],
    "rows" => []
  );
  $hasOtherLang = false;


  //  Ищем точное соответствие
  try {
    $query = mysql_query($mysql, "select id, en, ru, zh from ".DB_PREFIX."glossary where ".getLang($key[$n]["from"])."=?", [$key[$n]["value"]]);
    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
      for ($i = 0; $i < count($part["rows"]); $i++) {
        if ($part["rows"][$i]["id"] == $row["id"]) {
          break;
        }
      }
      if ($i < count($part["rows"])) { continue; }

      if (hasOtherLang($key[$n]["from"], $row)) { $hasOtherLang = true; }

      array_push(
        $part["rows"],
        array(
          "id" => $row["id"],
          "en" => $row["en"],
          "ru" => $row["ru"],
          "zh" => $row["zh"]
        )
      );
    }
    $query->closeCursor();
  } catch (Exception $e) {
    Finish($mysql);
    logAndExit($e->getMessage());
  }



  //  Ищем прочие совпадения

  try {
    $query = mysql_query($mysql, "select id, en, ru, zh from ".DB_PREFIX."glossary where ".getLang($key[$n]["from"])." like ?", [$key[$n]["value"]."%"]);
    while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
      for ($i = 0; $i < count($part["rows"]); $i++) {
        if ($part["rows"][$i]["id"] == $row["id"]) {
          break;
        }
      }
      if ($i < count($part["rows"])) { continue; }

      if (hasOtherLang(getLang($key[$n]["from"]), $row)) { $hasOtherLang = true; }

      array_push(
        $part["rows"],
        array(
          "id" => $row["id"],
          "en" => $row["en"],
          "ru" => $row["ru"],
          "zh" => $row["zh"]
        )
      );
    }
    $query->closeCursor();
  } catch (Exception $e) {
    Finish($mysql);
    logAndExit($e->getMessage());
  }





  //  Ищем прочие совпадения #2

  if (!$hasOtherLang) {
    try {
      $query = mysql_query($mysql, "select id, en, ru, zh from ".DB_PREFIX."glossary where ".getLang($key[$n]["from"])." like ?", ["%".$key[$n]["value"]."%"]);
      while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        for ($i = 0; $i < count($part["rows"]); $i++) {
          if ($part["rows"][$i]["id"] == $row["id"]) {
            break;
          }
        }
        if ($i < count($part["rows"])) { continue; }

        if (hasOtherLang(getLang($key[$n]["from"]), $row)) { $hasOtherLang = true; }

        array_push(
          $part["rows"],
          array(
            "id" => $row["id"],
            "en" => $row["en"],
            "ru" => $row["ru"],
            "zh" => $row["zh"]
          )
        );
      }
      $query->closeCursor();
    } catch (Exception $e) {
      Finish($mysql);
      logAndExit($e->getMessage());
    }
  }


  //  Ищем прочие совпадения #3

  if (!$hasOtherLang && (count(explode(" ", $key[$n]["value"])) > 1)) {
    $subrows = explode(" ", $key[$n]["value"]);

    for ($s = 0; $s < count($subrows); $s++) {
      $subrows[$s] = trim($subrows[$s]);
      if (empty($subrows[$s])) { continue; }
      try {
        $query = mysql_query($mysql, "select id, en, ru, zh from ".DB_PREFIX."glossary where ".getLang($key[$n]["from"])." = ?", [$subrows[$s]]);
        while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
          for ($i = 0; $i < count($part["rows"]); $i++) {
            if ($part["rows"][$i]["id"] == $row["id"]) {
              break;
            }
          }
          if ($i < count($part["rows"])) { continue; }

          if (hasOtherLang(getLang($key[$n]["from"]), $row)) { $hasOtherLang = true; }

          array_push(
            $part["rows"],
            array(
              "id" => $row["id"],
              "en" => $row["en"],
              "ru" => $row["ru"],
              "zh" => $row["zh"]
            )
          );
        }
        $query->closeCursor();
      } catch (Exception $e) {
        Finish($mysql);
        logAndExit($e->getMessage());
      }
    }
  }


  //  Ищем прочие совпадения #4
  if (!$hasOtherLang && (count(explode(" ", $key[$n]["value"])) == 1)) {
    try {
      $st1 = mb_substr($key[$n]["value"], 0, mb_strlen($key[$n]["value"], "UTF-8") - 1, "UTF-8");
      $st2 = mb_substr($key[$n]["value"], 0, mb_strlen($key[$n]["value"], "UTF-8") - 2, "UTF-8");
      $query = mysql_query($mysql, "select id, en, ru, zh from ".DB_PREFIX."glossary where ".getLang($key[$n]["from"])."=? or ".getLang($key[$n]["from"])."=?", [$st1, $st2]);
      while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
        for ($i = 0; $i < count($part["rows"]); $i++) {
          if ($part["rows"][$i]["id"] == $row["id"]) {
            break;
          }
        }
        if ($i < count($part["rows"])) { continue; }

        array_push(
          $part["rows"],
          array(
            "id" => $row["id"],
            "en" => $row["en"],
            "ru" => $row["ru"],
            "zh" => $row["zh"]
          )
        );
      }
      $query->closeCursor();
    } catch (Exception $e) {
      Finish($mysql);
      logAndExit($e->getMessage());
    }
  }



  array_push($translates, $part);
}


Finish($mysql);



//  Отдаем ответ


$response = array(
  "code" => "OK",
  "key" => $_POST["key"],
  "translates" => $translates
);

writeLog($translates);

echo json_encode($response, JSON_UNESCAPED_UNICODE);


//------------- Get Lang
function getLang($lang) {
  switch ($lang) {
    case 1: return "ru";
    case 2: return "zh";
  }
  return "en";
}


//------------- Write Log
function writeLog($translates) {
  try {
    $date = new DateTime();
    $filename = "logs/".$date->format("Y_m_d").".log";


    if (gettype($translates) == "array") {
      file_put_contents($filename, "\r\n\r\n".$date->format("d.m.Y H:i:s")." OK\r\n", FILE_APPEND);
      
      if ((count($translates) == 1) && (count($translates[0]["rows"]) < 1)) {
        file_put_contents($filename, "key: ".$translates[0]["key"].". Nothing found", FILE_APPEND);
      } else {
        for ($i = 0; $i < count($translates); $i++) {
          $content = (count($translates[$i]["rows"]) > 1) ? json_encode($translates[$i]["rows"][0], JSON_UNESCAPED_UNICODE)."; total: ".count($translates[$i]["rows"]) : json_encode($translates[$i]["rows"], JSON_UNESCAPED_UNICODE);
          file_put_contents(
            $filename,
            "key: ".$translates[$i]["key"]."; from: ".getLang($translates[$i]["from"])."; stRow: ".$content,
            FILE_APPEND
          );
        }
      }
    } else {
      file_put_contents($filename, "\r\n\r\n".$date->format("d.m.Y H:i:s")." ERROR ".$translates, FILE_APPEND);
    }

  } catch (Exception $e) {
    return;
  }
}


//------------- Log And Exit
function logAndExit($message) {
  writeLog($message);
  exit(json_encode(array("code" => "ERROR", "message" =>$message), JSON_UNESCAPED_UNICODE));
}


function hasOtherLang($key, $row) {
  if ($row["ru"] && (getLang($key) != "ru")) { return true; }
  if ($row["en"] && (getLang($key) != "en")) { return true; }
  if ($row["zh"] && (getLang($key) != "zh")) { return true; }
  return false;
}
?>
