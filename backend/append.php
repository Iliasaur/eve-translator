<?
require_once($_SERVER["DOCUMENT_ROOT"]."/et/system/info.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/et/system/utils.php");

//  Подготовка полей

if (!isset($_POST["value"])) {
	exit(json_encode(array("code" => "ERROR", "message" => "Пустая фраза"), JSON_UNESCAPED_UNICODE));
}

$_POST["id"] = isset($_POST["id"]) ? ValidateInteger($_POST["id"], 0, false) : 0;
$_POST["value"] = ValidateString($_POST["value"], 800, true);
$_POST["lang"] = isset($_POST["lang"]) ? ValidateString($_POST["lang"], 2, false) : "";

if ($_POST["id"] < 1) {
	exit(json_encode(array("code" => "ERROR", "message" => "Пустой идентификатор фразы"), JSON_UNESCAPED_UNICODE));
}

if (($_POST["lang"] != "en") && ($_POST["lang"] != "ru") && ($_POST["lang"] != "zh")) {
	exit(json_encode(array("code" => "ERROR", "message" => "Неправильный язык"), JSON_UNESCAPED_UNICODE));
}


//  Соединение с mySQL

$mysql = Start(true);




if ($_POST["lang"] != "en") {




  //  Ищем запись

  $en = "";

  /* PHP 7 Start
  try {
    $query = mysql_query($mysql, "select en from ".DB_PREFIX."glossary where id=?", [$_POST["id"]]);  
    $row = $query->fetch(PDO::FETCH_ASSOC);
    if ($row) { $en = $row["en"]; }
    $query->closeCursor();
  } catch (Exception $e) {
    Finish($mysql);
    exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
  }
  PHP 7 Finish */

  /* PHP 5.4 Start */
  try {
    $result = mysql_query("select en from ".DB_PREFIX."glossary where id=".$_POST["id"]) or die(mysql_error());
    if ($row = mysql_fetch_array($result)) {
      $en = $row["en"];
    }
    mysql_free_result($result);
  } catch (Exception $e) {
    Finish($mysql);
    exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
  }
  /* PHP 5.4 Finish */




  //  Избегаем дублей

  if ($en == $_POST["value"]) {
  	Finish($mysql);
    exit(json_encode(array("code" => "OK"), JSON_UNESCAPED_UNICODE));
  }
}



//  Вставляем запись

/* PHP 7 Start
try {
  mysql_query($mysql, "insert into ".DB_PREFIX."glossary (id, ".$_POST["lang"].") values (?, ?) on duplicate key update ".$_POST["lang"]."=?", [$_POST["id"], $_POST["value"], $_POST["value"]]);
} catch (Exception $e) {
  Finish($mysql);
  exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
}
PHP 7 Finish */

/* PHP 5.4 Start */
try {
  mysql_query("insert into ".DB_PREFIX."glossary (id, ".$_POST["lang"].") values (\"".$_POST["id"]."\", \"".$_POST["value"]."\") on duplicate key update ".$_POST["lang"]."=\"".$_POST["value"]."\"");
} catch (Exception $e) {
  Finish($mysql);
  exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
}
/* PHP 5.4 Finish */




Finish($mysql);

exit(json_encode(array("code" => "OK"), JSON_UNESCAPED_UNICODE));
?>
