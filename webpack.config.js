"use strict";


const NODE_ENV = process.env.NODE_ENV || "development";
const UI_LANG = "ru";
const webpack = require("webpack");

module.exports = [

  { //--- querier
    entry: {Querier: "./src/querier.js"},
    
    output: {path: __dirname + "/js", filename: "querier.js", library: "Querier"},

    watch: NODE_ENV == "development",
    
    watchOptions: {aggregateTimeout: 300},
    
    devtool: NODE_ENV == "development" ? "cheap-inline-module-source-map" : false,

    plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), UI_LANG: JSON.stringify(UI_LANG)})],

    module: {rules: [{test: /\.js$/, loader: "babel-loader", query: {presets: ["env"]}}, {test: /\.html$/, use: "raw-loader"}]}
  },


  { //--- settler
    entry: {Settler: "./src/settler.js"},
    
    output: {path: __dirname + "/js", filename: "settler.js", library: "Settler"},

    watch: NODE_ENV == "development",
    
    watchOptions: {aggregateTimeout: 300},
    
    devtool: NODE_ENV == "development" ? "cheap-inline-module-source-map" : false,

    plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), UI_LANG: JSON.stringify(UI_LANG)})],

    module: {rules: [{test: /\.js$/, loader: "babel-loader", query: {presets: ["env"]}}, {test: /\.html$/, use: "raw-loader"}]}
  },

/*

  { //--- parser en
    entry: {ParserEn: "./src/parser_en.js"},
    
    output: {path: __dirname + "/js", filename: "parser_en.js", library: "ParserEn"},

    watch: NODE_ENV == "development",
    
    watchOptions: {aggregateTimeout: 300},
    
    devtool: NODE_ENV == "development" ? "cheap-inline-module-source-map" : false,

    plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), UI_LANG: JSON.stringify(UI_LANG)})],

    module: {rules: [{test: /\.js$/, loader: "babel-loader", query: {presets: ["env"]}}, {test: /\.html$/, use: "raw-loader"}]}
  },



  { //--- parser ru
    entry: {ParserRu: "./src/parser_ru.js"},
    
    output: {path: __dirname + "/js", filename: "parser_ru.js", library: "ParserRu"},

    watch: NODE_ENV == "development",
    
    watchOptions: {aggregateTimeout: 300},
    
    devtool: NODE_ENV == "development" ? "cheap-inline-module-source-map" : false,

    plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), UI_LANG: JSON.stringify(UI_LANG)})],

    module: {rules: [{test: /\.js$/, loader: "babel-loader", query: {presets: ["env"]}}, {test: /\.html$/, use: "raw-loader"}]}
  },



  { //--- parser zh
    entry: {ParserZh: "./src/parser_zh.js"},
    
    output: {path: __dirname + "/js", filename: "parser_zh.js", library: "ParserZh"},

    watch: NODE_ENV == "development",
    
    watchOptions: {aggregateTimeout: 300},
    
    devtool: NODE_ENV == "development" ? "cheap-inline-module-source-map" : false,

    plugins: [new webpack.DefinePlugin({NODE_ENV: JSON.stringify(NODE_ENV), UI_LANG: JSON.stringify(UI_LANG)})],

    module: {rules: [{test: /\.js$/, loader: "babel-loader", query: {presets: ["env"]}}, {test: /\.html$/, use: "raw-loader"}]}
  }*/
]

if (NODE_ENV == "production") {
  module.exports.forEach(function(item, i, arr) {
    item.plugins.push(new webpack.optimize.UglifyJsPlugin({compress: {warnings: false, drop_console: true, unsafe: true}}));
  })
}

