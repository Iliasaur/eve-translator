import dict from "../json/en.json"

class ParserEn {

  //------------- Конструктор
  constructor() {
    this.lang = "en"
    this.total = dict.length
    this.done = 0
    this.current = 0
    this.$status = $("#status")

    $("#start").on("click", () => { this.start() })
  }


  //------------- Start
  start() {
    /*
    for (let word1 of dict) {
      if (word1.isd) { continue; }
      for (let word2 of dict) {
        if (word2.isd || (word1.id == word2.id)) { continue; }
        if (word1.val == word2.val) { word2.isd = true }
      }
    }

    let a = []
    for (let word of dict) {
      if (word.isd) { continue; }
      a.push({i: word.id, v: word.val})
    }

    this.$status.text(JSON.stringify(a))
    */

    this.current = 0;
    this._append();
  }


  //------------- Append
  _append() {
    this.current++

    if (this.current > dict.length) {
      this.$status.text("Done: " + this.done + " of " + this.total)
      return
    }

    let dataSet = {
      id: dict[this.current - 1].i,
      value: dict[this.current - 1].v,
      lang: this.lang
    }

    $.ajax({
      url: "backend/append.php",
      type: "POST",
      data: dataSet,
      dataType: "json",
      
      success: (data) => {
        if ((typeof(data) == "object") && (data != null) && (data.code == "OK")) {
          this.done++
          this.$status.text(this.current + " of " + this.total)
          this._append()
        } else {
          let message = ((typeof(data) == "object") && (data != null) && (typeof(data.message) == "string")) ? $.trim(data.message) : ""
          this.$status.text(message ? message : "Unknown error")
        }
      },
      
      error: (XMLHttpRequest, textStatus, errorThrown) => {
        console.log(XMLHttpRequest, textStatus, errorThrown)
        this.$status.text(errorThrown ? errorThrown : "Ajax error")
      }
    });
  }

  
}

export default ParserEn
