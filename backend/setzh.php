<?
require_once($_SERVER["DOCUMENT_ROOT"]."/system/info.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/system/utils.php");

//  Подготовка полей

if (!isset($_POST["value"])) {
	exit(json_encode(array("code" => "ERROR", "message" => "Пустая фраза"), JSON_UNESCAPED_UNICODE));
}

$_POST["id"] = isset($_POST["id"]) ? ValidateInteger($_POST["id"], 0, false) : 0;
$_POST["value"] = ValidateString($_POST["value"], 800, true);

if ($_POST["id"] < 1) {
	exit(json_encode(array("code" => "ERROR", "message" => "Пустой идентификатор фразы"), JSON_UNESCAPED_UNICODE));
}


//  Соединение с mySQL

$mysql = Start(true);


//  Вставляем запись


try {
  mysql_query($mysql, "insert into ".DB_PREFIX."glossary (id, zh) values (?, ?) on duplicate key update zh=?", [$_POST["id"], $_POST["value"], $_POST["value"]]);
} catch (Exception $e) {
  Finish($mysql);
  exit(json_encode(array("code" => "ERROR", "message" => $e->getMessage()), JSON_UNESCAPED_UNICODE));
}



Finish($mysql);

exit (json_encode(array("code" => "OK"), JSON_UNESCAPED_UNICODE));
?>
