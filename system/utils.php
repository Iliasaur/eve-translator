<?

//---------------- Возвращает ENCODING
function Encoding() {
  return ((defined("ENCODING") and !empty(ENCODING)) ? ENCODING : "UTF-8");
}

//------------------------------------ Запрос mySQL на выборку
function mysql_query($mysql, $qr, $params) {
  try {
    $query = $mysql->prepare($qr);
  } catch (Exception $e) {
    throw new Exception($e->getMessage());
  }

  if (!$query) {
    $err = $mysql->errorInfo();
    throw new Exception("Не удалось подготовить SQL-запрос (".($err[2] ? $err[2] : ($err[1] ? $err[1] : $err[0])).", ".gettype($query)."): ".$qr);
  }

  $result = $query->execute($params);
  if ($result === false) {
    $err = $mysql->errorInfo();
    throw new Exception("Не удалось выполнить SQL-запрос (".($err[2] ? $err[2] : ($err[1] ? $err[1] : $err[0])).", ".gettype($query)."): ".$qr);
  }

  return $query;
}


//------------------------------------ Запускает сессию
function Start($connectMySQL) {
  $locale = defined("LOCALE") and !empty(LOCALE) ? LOCALE : "ru_RU.UTF-8";
  SetLocale(LC_ALL, $locale);

  mb_internal_encoding(Encoding());
  mb_regex_encoding(Encoding());
  mb_http_output(Encoding());

  if ($connectMySQL) {
    if (!class_exists("PDO")) { die("PHP-класс PDO не установлен"); }
    try {
      $mysql = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_LOGIN, dsCrypt(DB_PASSWORD, true));
      if (!$mysql) { die("Не удалось соединиться с базой данных"); }
      $query = $mysql->prepare("SET NAMES utf8");
      $query->execute();
      $query = $mysql->prepare("SET LOCAL time_zone=\"".date("P")."\"");
      $query->execute();
    } catch(Exception $e) { die($e->getMessage()); }
  }

  session_start();

  return $mysql;
}


//------------------------------------- Завершает сессию
function Finish($mysql) {
  $mysql = null;
}	


//------------- Валидация строки
function ValidateString($value, $length, $add_slashes) {
  if (!empty($value)) {
    $value = StripSlashes((string)$value);
    $value = Chop($value);
    if ($add_slashes) {
      $value = str_replace("\"", "&quot;", $value);
      $value = AddSlashes($value);
    }
    if ($length > 0) { $value = mb_substr($value, 0, $length); }
    return $value;
  }
  return "";
}


//------------- Валидация целого числа
function ValidateInteger($value, $min, $max) {
  $result = (integer)$value;
  if (!($max === false)) {
    $max = (integer)$max;  
    if ($result > $max) { $result = $max; }
  }
  if (!($min === false)) {
    $min = (integer)$min;
    if ($result < $min) { $result = $min; }
  }
  return $result;
}

//------------------------------------- Integer To Binary
function IntToBin($I) {
  $ret = "";
  while ($I > 0) {
    $ret = (($I / 2) != round($I / 2)) ? "1".$ret : "0".$ret;
    $I = floor($I / 2); 
  }
  return $ret;
}


//------------------------------------- Binary To Integer
function BinToInt($S) {
  $ret = 0;
  for($n = 1; $n <= mb_strlen($S); $n++) {
    if ($n <> mb_strlen($S)) {
      if (mb_substr($S, $n - 1, 1) == '1') {
        $r = 2;
        for ($c = 1; $c <= (mb_strlen($S) - $n - 1); $c++) { $r = $r * 2; }
        $ret += $r; 
      }   
    } else {
      if (mb_substr($S, mb_strlen($S) - 1, 1) == '1') { $ret++; }
    } 
  }
  return $ret;
}


//------------------------------------- Обратимое шифрование методом "Двойного квадрата" (Reversible crypting of "Double square" method)
function dsCrypt($input, $decrypt = false) {
  $o = $s1 = $s2 = array();
  $basea = array('?','(','@',';','$','#',"]","&",'*');
  $basea = array_merge($basea, range('a','z'), range('A','Z'), range(0, 9));
  $basea = array_merge($basea, array('!',')','_','+','|','%','/','[','.',' '));
  $dimension = 9;
  for ($i = 0; $i < $dimension; $i++) {
    for ($j = 0; $j < $dimension; $j++) {
      $s1[$i][$j] = $basea[$i * $dimension + $j];
      $s2[$i][$j] = str_rot13($basea[($dimension * $dimension - 1) - ($i * $dimension + $j)]);
    }
  }
  unset($basea);
  $m = floor(mb_strlen($input) / 2) * 2;
  $symbl = $m == mb_strlen($input) ? '':$input[mb_strlen($input) - 1];
  $al = array();
  for ($ii = 0; $ii < $m; $ii+=2) {
    $symb1 = $symbn1 = strval($input[$ii]);
    $symb2 = $symbn2 = strval($input[$ii + 1]);
    $a1 = $a2 = array();
    for ($i = 0; $i < $dimension; $i++) {
      for($j = 0; $j < $dimension; $j++) {
        if ($decrypt) {
          if ($symb1 === strval($s2[$i][$j])) { $a1 = array($i, $j); }
          if ($symb2 === strval($s1[$i][$j])) { $a2 = array($i, $j); }
          if (!empty($symbl) && ($symbl === strval($s2[$i][$j]))) { $al = array($i, $j); }
        } else {
          if ($symb1 === strval($s1[$i][$j])) { $a1 = array($i, $j); }
          if ($symb2 === strval($s2[$i][$j])) { $a2 = array($i, $j); }
          if (!empty($symbl) && ($symbl === strval($s1[$i][$j]))) { $al = array($i, $j); }
        }
      }
    }
    if (sizeof($a1) && sizeof($a2)) {
      $symbn1 = $decrypt ? $s1[$a1[0]][$a2[1]] : $s2[$a1[0]][$a2[1]];
      $symbn2 = $decrypt ? $s2[$a2[0]][$a1[1]] : $s1[$a2[0]][$a1[1]];
    }
    $o[] = $symbn1.$symbn2;
  }
  if (!empty($symbl) && sizeof($al)) {
    $o[] = $decrypt ? $s1[$al[1]][$al[0]] : $s2[$al[1]][$al[0]];
  }
  return implode('', $o);
}


//------------------------------------- DecryptPASS
function Decrypt($PASS) {
  $key = 11;
  $ret = "";
  $C1 = 52845;
  $C2 = 22719;
  for ($i = 1; $i <= mb_strlen($PASS); $i++) {
    $ret .= Chr(Ord(mb_substr($PASS, ($i - 1), 1)) ^ ($key >> 8));
    $key = BinToInt(mb_substr(IntToBin((Ord(mb_substr($PASS, ($i - 1), 1)) + $key) * $C1 + $C2), -16, 16));
  }
  return $ret;
}
?>
